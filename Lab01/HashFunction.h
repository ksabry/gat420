#pragma once

#include <cstdint>

uint32_t hashStringPrime(const char * const string);