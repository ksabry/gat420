#pragma once

template<typename T>
class Node
{
public:
    Node()
        : tail(nullptr)
    {
    }
    ~Node()
    {
        if (tail != nullptr)
            delete tail;
    }

    T Head()
    {
        return head;
    }

    Node<T>* Tail()
    {
        return tail;
    }

    void SetHead(T value)
    {
        head = value;
    }

    void SetTail(Node<T>* value)
    {
        tail = value;
    }

private:
    T head;
    Node<T>* tail;
};

template<typename T>
class LinkedList
{
public:
    LinkedList(std::function<bool(T, T)> equality = [](T a, T b) { return a == b; })
        : node(nullptr), equality(equality), length(0)
    {
    }
    ~LinkedList()
    {
        delete node;
    }

    T Get(size_t index)
    {
        return GetNode(node, index);
    }

    void Set(size_t index, T value)
    {
        return SetNode(node, index);
    }

    void Insert(size_t index, T value)
    {
        auto newNode = InsertNode(node, index, value);
        if (index == 0)
            node = newNode;
    }

    size_t Count()
    {
        return length;
    }

    bool Contains(T value)
    {
        return ContainsNode(node, value);
    }

private:
    Node<T>* node;
    std::function<bool(T, T)> equality;
    size_t length;

    void GetNode(Node<T>* node, size_t index)
    {
        if (index == 0)
            return node->Head();
        else
            return GetNode(node->Tail(), index - 1);
    }

    void SetNode(Node<T>* node, size_t index, T value)
    {
        if (index == 0)
            node->head = value;
        else
        {
            if (node->Tail() == nullptr)
            {
                node->SetTail(new Node<T>());
                length++;
            }
            SetNode(node->Tail(), index - 1, value);
        }
    }

    Node<T>* InsertNode(Node<T>* node, size_t index, T value)
    {
        if (index == 0)
        {
            if (node == nullptr)
            {
                node = new Node<T>();
                node->SetHead(value);
            }
            else
            {
                auto next = new Node<T>();
                next->SetHead(node->Head());
                next->SetTail(node->Tail());
                node->SetHead(value);
                node->SetTail(next);
            }
            length++;
        }
        else
        {
            if (node->Tail() == nullptr)
            {
                node->SetTail(new Node<T>());
                length++;
            }
            InsertNode(node->Tail(), index - 1, value);
        }
        return node;
    }

    bool ContainsNode(Node<T>* node, T value)
    {
        if (node == nullptr)
            return false;
        if (equality(node->Head(), value))
            return true;
        else
            return ContainsNode(node->Tail(), value);
    }
};