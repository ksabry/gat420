#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <string>
#include <chrono>

#include "HashFunction.h"
#include "HashTable.h"

bool IsValidWord(const char * word)
{
    if (word[0] == '\'')
        return false;
    
    for (const char* c = word; *c != '\0'; c++)
    {
        if (*c == '\'' || (*c >= 'a' && *c <= 'z') || (*c >= 'A' && *c <= 'Z'))
            continue;
        return false;
    }
    return true;
}

bool InsertWordsFromFile(const char * const filename, HashTable<char*>& hashSet)
{
    std::ifstream file;
    file.open(filename);
    if (!file.is_open())
        return false;

    unsigned count = 0;
    char* words = new char[4000000];
    char* next_word = words;
    char word[256];
    
    auto start = std::chrono::high_resolution_clock::now();

    while (file >> word)
    {
        size_t length = strlen(word);
        strcpy_s(next_word, length + 1, word);
        next_word[length] = '\0';
        
        if (IsValidWord(next_word))
        {
            hashSet.Insert(next_word);
            count++;
        }
        next_word += length + 1;
    }

    auto end = std::chrono::high_resolution_clock::now();
    long long nanos = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    printf("Inserted %d words in %lld nanoseconds\nAverage of %lld nanoseconds per insert\n", count, nanos, nanos / count);

    return true;
}

bool FindWordsFromFile(const char * const filename, HashTable<char*>& hashSet)
{
    std::ifstream file;
    file.open(filename);
    if (!file.is_open())
        return false;

    unsigned count = 0;
    char* words = new char[360000 * 256];
    char word[256];
    while (file >> word)
        strcpy_s(words + 256 * (count++), 256, word);

    unsigned found = 0;
    std::cout << "-----" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    
    for (size_t i = 0; i < count; i++)
    {
        if (hashSet.Contains(words + 256 * i))
            found++;
        else
            std::cout << (words + 256 * i) << std::endl;
    }
    auto end = std::chrono::high_resolution_clock::now();
    long long nanos = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    std::cout << "-----" << std::endl;
    printf("Found %d out of %d words in %lld nanoseconds\nAverage of %lld nanoseconds per find\n", found, count, nanos, nanos / count);

    return true;
}

uint32_t hashStringZero(const char * const)
{
    return 0;
}

uint32_t hashStringFirstChar(const char * const string)
{
    return string[0];
}

uint32_t hashStringFirstThree(const char * const string)
{
    size_t length = strlen(string);
    return string[0] + (length > 1 ? string[1] : 0) + (length > 2 ? string[2] : 0);
}

uint32_t hashStringXor(const char * const string)
{
    uint32_t result = 0;
    size_t length = strlen(string);
    for (size_t i = 0; i < length; i++)
        result ^= string[i] << i;
    return result;
}

uint32_t hashStringPrime(const char * const string)
{
    uint32_t hash = 0;
    for (const char * c = string; *c != '\0'; c++)
        // 257 is the first prime above 256
        hash = hash * 13 + *c + 11;

    return hash;
}

#include "SHA2.h"
uint32_t hashStringSHA2(const char * const string)
{
    BYTE buffer[SHA256_BLOCK_SIZE];
    SHA256_CTX ctx;

    sha256_init(&ctx);
    sha256_update(&ctx, (BYTE*)string, strlen(string));
    sha256_final(&ctx, buffer);

    uint32_t result = 0;
    result ^= *(uint32_t*)(buffer);
    result ^= *(uint32_t*)(buffer + 8);
    result ^= *(uint32_t*)(buffer + 16);
    result ^= *(uint32_t*)(buffer + 24);

    return result;
}

int main(int argc, char** argv)
{
    if (argc < 3)
    {
        std::cout << "Must provide filename and bucket size in command line arguments";
        std::cin.ignore();
        return 0;
    }
    
    char* filename = argv[1];
    int bucketSize = std::stoi(argv[2]);

    HashTable<char*> hashTable(bucketSize, &hashStringSHA2, [](char* a, char* b) { return strcmp(a, b) == 0; });
    
    InsertWordsFromFile(filename, hashTable);
    hashTable.DisplayStats();
    
    std::cin.ignore();
}
