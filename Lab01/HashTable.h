#pragma once

#include <functional>
#include "LinkedList.h"

template<typename T>
class HashTable
{
public:
    HashTable(size_t bucketCount, std::function<uint32_t(T)> hashFunction, std::function<bool(T, T)> equality = [](T a, T b) { return a == b; })
        : bucketCount(bucketCount), hashFunction(hashFunction), equality(equality), length(0), attemptedDuplicates(0)
    {
        buckets = new LinkedList<T>*[bucketCount];
        for (size_t i = 0; i < bucketCount; i++)
            buckets[i] = nullptr;
    }
    ~HashTable()
    {
        for (size_t i = 0; i < bucketCount; i++)
            if (buckets[i] != nullptr)
                delete buckets[i];
        delete[] buckets;
    }

    bool Insert(T key)
    {
        if (Contains(key))
        {
            attemptedDuplicates++;
            return false;
        }

        uint32_t index = hashFunction(key) % bucketCount;

        if (buckets[index] == nullptr)
            buckets[index] = new LinkedList<T>(equality);
        buckets[index]->Insert(0, key);
        
        length++;
        return true;
    }

    bool Contains(T key)
    {
        int32_t index = hashFunction(key) % bucketCount;
        
        if (buckets[index] == nullptr)
            return false;
        return buckets[index]->Contains(key);
    }

    size_t Count()
    {
        return length;
    }

    size_t AttemptedDuplicates()
    {
        return attemptedDuplicates;
    }

    void DisplayStats()
    {
        size_t count = Count();
        double ideal = (double)count / bucketCount;

        double totalDifferenceSquared = 0;
        double totalDifference = 0;

        size_t minimumBucketSize = 100000000;
        size_t maximumBucketSize = 0;
        size_t minimumBucketCount = 0;
        size_t maximumBucketCount = 0;

        for (size_t i = 0; i < bucketCount; i++)
        {
            size_t bucketLength = buckets[i] == nullptr ? 0 : buckets[i]->Count();
            double difference = abs(ideal - bucketLength);
            totalDifference += difference;
            totalDifferenceSquared += difference * difference;

            if (bucketLength < minimumBucketSize)
            {
                minimumBucketSize = bucketLength;
                minimumBucketCount = 1;
            }
            else if (bucketLength == minimumBucketSize)
            {
                minimumBucketCount++;
            }

            if (bucketLength > maximumBucketSize)
            {
                maximumBucketSize = bucketLength;
                maximumBucketCount = 1;
            }
            else if (bucketLength == maximumBucketSize)
            {
                maximumBucketCount++;
            }
        }
        double averageDifference = totalDifference / bucketCount;
        double averageDifferenceSquared = totalDifferenceSquared / bucketCount;

        printf("Entries Count: %ud\n", count);
        printf("Bucket Count: %ud\n", bucketCount);
        printf("Ideal words per bucket: %f\n", (double)count / bucketCount);
        printf("Average difference from ideal: %f\n", averageDifference);
        printf("Variance: %f\n", averageDifferenceSquared);
        printf("Standard deviation: %f\n", sqrt(averageDifferenceSquared));
        printf("Minimum bucket size is %d with %d buckets\n", minimumBucketSize, minimumBucketCount);
        printf("Maximum bucket size is %d with %d buckets\n", maximumBucketSize, maximumBucketCount);
    }

    void BucketSizes()
    {
        std::cout << "Bucket sizes:\n";
        for (size_t i = 0; i < bucketCount; i++)
        {
            size_t count = buckets[i] == nullptr ? 0 : buckets[i]->Count();
            std::cout << count << " ";
            if (i % 32 == 0)
                std::cout << std::endl;
        }
    }

private:
    size_t bucketCount;
    LinkedList<T>** buckets;

    std::function<uint32_t(T)> hashFunction;
    std::function<bool(T, T)> equality;
    
    size_t length;
    size_t attemptedDuplicates;
};